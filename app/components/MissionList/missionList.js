import NormalA from 'components/A';
import styled from 'styled-components';

export default styled(NormalA)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
