import React from 'react';
import PropTypes from 'prop-types';
import List from './missionList';

function MissionList({ missions }) {
  if (missions) {
    return missions.map(mission => (
      <List>
        <a target="_blank" href={mission.links.video_link}>
          {mission.mission_name}
        </a>
      </List>
    ));
  }

  return null;
}

MissionList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  missions: PropTypes.any,
};

export default MissionList;
