/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { CHANGE_USERNAME, UPDATE_MISSION_LIST, SET_ERROR_MESSAGE } from './constants';

// The initial state of the App
export const initialState = {
  username: '',
  missionList: [],
  errorMessage: '',
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_USERNAME:
        // Delete prefixed '@' from the github username
        draft.username = action.username.replace(/@/gi, '');
        break;
      case UPDATE_MISSION_LIST:
        // Delete prefixed '@' from the github username
        draft.missionList = action.missionList;
        break;
      case SET_ERROR_MESSAGE:
        // Delete prefixed '@' from the github username
        draft.errorMessage = action.errorMessage;
        break;
    }
  });

export default homeReducer;
