/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );

const makeSelectMissionList = () =>
  createSelector(
    selectHome,
    homeState => homeState.missionList,
  );

export { selectHome, makeSelectUsername, makeSelectMissionList };
