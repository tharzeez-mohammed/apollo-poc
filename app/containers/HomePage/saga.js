/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import gql from 'graphql-tag';
import { LOAD_REPOS, LOAD_MISSION_LIST } from 'containers/App/constants';
import { GET_MISSION_LIST } from './constants';
import { setErrorMessage, updateMissionList } from './actions';

import graphqlClient from '../../utils/services/graphql-client';
import {
  reposLoaded,
  repoLoadingError,
} from 'containers/App/actions';

import request from 'utils/request';
import { missionListQuery } from './queries/missionList';
import {
  makeSelectUsername,
  makeSelectMissionList,
} from 'containers/HomePage/selectors';

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Github repos request/response handler
 */
export function* getMissionListSaga() {
  try {
    const { data } = yield call(graphqlClient.query, {
      query: gql`
        ${missionListQuery}
      `,
      variables: {
        limit: 10
      },
      // fetchPolicy: 'network-only',
    });
    yield put(updateMissionList(data.launchesPast));
  } catch (err) {
    yield put(setErrorMessage(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_REPOS, getRepos);
  yield takeLatest(GET_MISSION_LIST, getMissionListSaga);
}
