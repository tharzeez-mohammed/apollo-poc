export const missionListQuery = `query MissionList($limit: Int) {
  launchesPast(limit: $limit) {
    mission_name
    launch_date_local
    launch_site {
      site_name_long
    }
    links {
      article_link
      video_link
    }
    ships {
      name
      home_port
      image
    }
  }
}`;
