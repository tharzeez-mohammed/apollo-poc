import { ApolloClient } from 'apollo-client';
// import 'cross-fetch/polyfill';
import { ApolloLink } from 'apollo-link';
import {
  InMemoryCache,
} from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { setContext } from 'apollo-link-context';

const setHeaders = setContext(async (_, { headers }) => ({
  headers: {
    ...headers,
    authorization: `Bearer AccessToken`,
  },
}));

export const errorLink = onError(
  ({ graphQLErrors, networkError, operation, forward }) => {
    console.log('graphql error occured')
  },
);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([
    errorLink,
    setHeaders,
    new HttpLink({
      uri: 'https://api.spacex.land/graphql',
    }),
  ]),
});

export default client;
